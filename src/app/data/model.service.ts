import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
const url = 'https://jsonplaceholder.typicode.com/todos'

@Injectable({
  providedIn: 'root'
})
export class ModelService {
  private dbPath = '/items';
  customersRef: AngularFireList<any> = null;
  constructor(public http: HttpClient,
              private db: AngularFireDatabase) { 
      this.customersRef = db.list(this.dbPath);
    }

    createCustomer(item: any): void {
      this.customersRef.push(item);
    }
   
    updateCustomer(key: string, value: any): Promise<void> {
      return this.customersRef.update(key, value);
    }
   
    deleteCustomer(key: string): Promise<void> {
      return this.customersRef.remove(key);
    }
   
    getCustomersList(): AngularFireList<any> {
      return this.customersRef;
    }  

    list(): Observable<any> {
      return this.http.get(url)
    }

}
