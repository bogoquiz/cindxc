import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ModelService } from '../data/model.service';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.scss']
})
export class CreateItemComponent implements OnInit {
  id:any;
  title:any;
  submitted = false;
  customer:any;
  constructor(private model: ModelService) { }

  ngOnInit() {
  }

  newCustomer(): void {
    this.submitted = false;
    this.id = ''
    this.title = ''
  }
 
  save() {
    this.model.createCustomer({
      id:this.id,
      title:this.title
    });
    this.id = ''
    this.title = ''
  }
 
  onSubmit() {
    this.submitted = true;
    this.save();
  }  
}
