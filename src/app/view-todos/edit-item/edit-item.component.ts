import { Component, OnInit, Input } from '@angular/core';
import { ModelService } from '../../data/model.service';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.scss']
})
export class EditItemComponent implements OnInit {
  @Input() item:any  
  constructor(private model: ModelService) { }

  ngOnInit() {
  }

  updateActive(isActive: boolean) {
    this.model
      .updateCustomer(this.item.key, { active: isActive })
      .catch(err => console.log(err));
  }
 
  deleteCustomer() {
    this.model
      .deleteCustomer(this.item.key)
      .catch(err => console.log(err));
  }

}
