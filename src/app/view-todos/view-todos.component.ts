import { Component, OnInit } from '@angular/core';
import { ModelService } from '../data/model.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-view-todos',
  templateUrl: './view-todos.component.html',
  styleUrls: ['./view-todos.component.scss']
})
export class ViewTodosComponent implements OnInit {
  customers: any;
  constructor(public model: ModelService) { }

  ngOnInit() {
    this.getCustomersList();
  }

  getCustomersList() {
  this.model.getCustomersList().snapshotChanges().pipe(
    map(changes =>
      changes.map(c =>
        ({ key: c.payload.key, ...c.payload.val() })
      )
    )
  ).subscribe(customers => {
    this.customers = customers;
  });
}


}
